# Python for security project
This is our academic project for our course of "Python for security".

## What is our project ?
This project is an **public Identity Provider** (IDP). The main goal is to have a **passwordless IDP**.

### What is a public IDP
An [IDP or Identity provider](https://en.wikipedia.org/wiki/Passwordless_authentication) is **a system that manages identity** on a shared network and **also provides authentication and identification** for applications. It's behavior is to centralize all identities, most of time for users. It's a highly secured system most of the time because **it contains all the users' private information**.

A public IDP is the same thing but it's... public. Any application can implement it like the "Connect with facebook" or "Connect with Google" buttons. Best of all, we'll do it as a systematic passwordless authentication !

### What we call systematic authentication
Each time an application want's to connect with our IDP, you'll need to grant it access.

### What is passwordless authentication
[Passwordless authentication](https://en.wikipedia.org/wiki/Passwordless_authentication) is an authentication without a password to keep **for the user**. That means, it's not really a "passwordless" as you can have one-time password. But **the user doesn't have to remember it** ! 

## Why ?
**Why passwordless ?** 

Most of the time, passwords are the weakest point of a system. They are chosen by the user.
Just lookinbg at the [most used passwords of 2020](https://nordpass.com/most-common-passwords-list/) shows that they can be so weak that it's ridiculous. Passwordless authentication is the way to get rid of bad passwords because the user now have zero choice on the security.

Why an IDP ?

Well... it's just more fun that sending an SMS an saying that our project is finished. Moreover, it's also a question of privacy. You know don't have to have user's personal data all over the globe, it's just centralized in one (ultra-protected we assume) database. And also, it's good for user experience not to have to change it's data on each website, application and so on. With a public IDP, users will just have to change their information on the IDP.


## What we will (maybe) do

__A passwordless IDP with the following authentication:__

- email link (that's just the proof of concept because an email is not a passwordless as you need to connect to your email using a password... But, hey, that's a starting point.)
- SMS one-time password (maybe. That's expensive to send SMS)
- Using a mobile application (maybe. Hard to do that in time...):
    - push notification
    - fingerprint
    
__An API with the following features:__
- Create an account (used directly by users)
- Modify an account (used directly by users)
- Connect to get personal data (used by applications)

## What we will NOT DO (but could have really make that project finished)

- Application and/or website to connect and modify user's account

    

