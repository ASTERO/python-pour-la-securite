from datetime import datetime

from flask import request, Response, json
from mysql.connector import IntegrityError

import src.utils.request_verifier as json_utils
from src.error.errors import invalid_date, user_already_exists, unauthorized
from src.objectpool import ObjectPool
from src.utils.date import minus_years
from src.utils.secure_randoms import gen_secure_rand_token

# allowed languages
LANGUAGES = 'hindi|french|german|english_eu|english_uk|english_au|italian|chinese|spanish|japanese|vietnamese|russian|portuguese'
LANGUAGES_NUMBER = len(LANGUAGES.split('|'))

# allowed nationalities
NATIONALITIES = 'indian|french|german|american|english|australian|italian|chinese|spanish|japanese|vietnamese|russian|portuguese'

# allowed genders
GENDERS = 'male|female|others'

# request body for POST /users
USER_REQUEST_BODY = \
    [
        {
            'name': 'msisdn',
            'required': False,
            'regex': r'[0-9]{9,15}'
        },
        {
            'name': 'lastname',
            'required': False,
            'regex': r"[a-zA-Z'\-éèëêàçôñîï]{1,100}"
        },
        {
            'name': 'firstname',
            'required': False,
            'regex': r"[a-zA-Z'\-éèëêàçôñîï]{1,100}"
        },
        {
            'name': 'email',
            'required': True,
            'regex': r'[^@]+@[^@]+\.[^@]+'
        },
        {
            'name': 'birthDate',
            'required': False,
            'regex': r'[0-9]{4}-[0-9]{2}-[0-9]{2}'  # basic regex, but we'll validate the date with datetime
        },
        {
            'name': 'languages',
            'required': False,
            # list of possible languages with ' ' separator with a maximum of the number of languages
            # (no problem if there is the same scope multiple times)
            'regex': r"((" + LANGUAGES + ") ){1," + str(LANGUAGES_NUMBER - 1) + "}(" + LANGUAGES + "){1}"
        },
        {
            'name': 'nationality',
            'required': False,
            'regex': NATIONALITIES
        },
        {
            'name': 'address',
            'required': False,
            'regex': r".{1, 500}"
        },
        {
            'name': 'gender',
            'required': False,
            'regex': GENDERS
        }

    ]


class UsersController:
    """class UsersController: class that will manage all requests with base path '/users'

    """
    def __init__(self, pool: ObjectPool):
        """constructor

        :param pool: the object pool: ObjectPool
        """
        self.pool = pool

    def get_info(self) -> Response:
        """get_info function: controller for GET /users/info. Returns the user's information according to the scopes
                                of the given token (in the Authorization header)

        :return: the response to send back to the client
        """

        # verify given authorization and get scopes and user ID
        scopes_and_userid = self.pool.security.verify_authorization(request.headers.get('Authorization'))
        if scopes_and_userid is None:
            return unauthorized()

        scopes = scopes_and_userid[0]
        user_id = scopes_and_userid[1]

        # get and return user's info
        res = self.pool.user_db.get_user_info(user_id, scopes)
        return Response(json.dumps(res), status=200, mimetype="application/json")

    def create_user(self) -> Response:
        """create_user function: controller for POST /users

            :return: the response to send back to the client
        """

        # verify input
        validation = json_utils.verify_dict_body(request.json, USER_REQUEST_BODY)
        if validation is not None:
            return validation
        birth_date = request.json["birthDate"]
        b_date_split = birth_date.split('-')
        try:
            b_date_datetime = datetime(year=int(b_date_split[0]),
                                       month=int(b_date_split[1]),
                                       day=int(b_date_split[2]))
            if b_date_datetime > minus_years(13, datetime.now()):
                return invalid_date("birthDate")
        except ValueError:
            return invalid_date("birthDate")

        # try to create user, return corresponding error if email or phone number already exists
        try:
            user_id = self.pool.user_db.create_user(request.json)
        except IntegrityError as e:
            if e.errno == 1062:
                if e.msg.endswith("users.EMAIL_UNIQUE'"):
                    return user_already_exists("email")
                elif e.msg.endswith("users.PHONE_NUMBER_UNIQUE'"):
                    return user_already_exists("msisdn")
            raise e

        # generate and save user token
        user_token = gen_secure_rand_token(250)
        self.pool.user_tokens_db.create_token(user_token, user_id)

        # return created user and corresponding user token
        user = request.json
        user["userToken"] = user_token
        return Response(json.dumps(user), status=201, mimetype="application/json")

    # def modify_user(self) -> Response:
    #     """create_user function: controller for PATCH /users
    #
    #         :return: the response to send back to the client
    #     """
    #     validation = json_utils.verify_dict_body(request.json, USER_REQUEST_BODY, False)
    #     if validation is not None:
    #         return validation
    #     # TODO: authentication, modification
    #     return Response(status=200)
