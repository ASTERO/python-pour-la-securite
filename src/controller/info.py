from flask import json


class InfoController:
    """class InfoController: class that will manage all requests with base path '/info'

    """

    def __init__(self, config_infos: dict):
        """constructor: initializes the information of the API

        :param config_infos: the information of the API as taken from the configuration file in [INFO] section
        """

        config_infos["maintenance"] = False if config_infos["maintenance"] in ['false', 'False', '0', 'FALSE'] else True
        self.infos = config_infos

    def get_infos(self) -> str:
        """get_infos function: controller for GET /infos. Get the infos of the API

        :return: the response as a string (JSON)
        """
        return json.dumps(self.infos)
