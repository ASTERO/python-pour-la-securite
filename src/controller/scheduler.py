from apscheduler.schedulers.background import BackgroundScheduler


class Scheduler:
    """class Scheduler: class that will manage all scheduled tasks

    """

    def __init__(self, pool, config: dict):
        """constructor: initializes the scheduler

        :param pool: the ObjectPool that has all instantiated objects
        :param config: the information of the Scheduler as taken from the configuration file in [SCHEDULER] section
        """
        # init scheduler
        self.scheduler = BackgroundScheduler()

        # init pool
        self.pool = pool

        # create job to delete expired codes (pending requests)
        self.scheduler.add_job(id='Delete expired codes',
                               func=self.delete_expired_codes,
                               trigger="interval",
                               minutes=int(config['interval_deletion_codes_minutes']))

        # create job to delete expired tokens (allowed requests)
        self.scheduler.add_job(id='Delete expired tokens',
                               func=self.delete_expired_tokens,
                               trigger="interval",
                               minutes=int(config['interval_deletion_tokens_minutes']))
        # start scheduler
        self.scheduler.start()

    def delete_expired_codes(self):
        """Scheduled method to delete expired codes (pending requests)

        :return: nothing
        """
        self.pool.codes_db.delete_expired(self.pool.time_limit_codes)

    def delete_expired_tokens(self):
        """Scheduled method to delete expired tokens (allowed requests)

        :return: nothing
        """
        self.pool.access_tokens_db.delete_expired(self.pool.time_limit_tokens)
