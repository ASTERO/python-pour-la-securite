from datetime import datetime, timedelta

from flask import Response, request, json, render_template

from src.error.errors import unknown_user, unauthorized, too_many_requests
from src.objectpool import ObjectPool
from src.utils.request_verifier import verify_dict_body, verify_query
from src.utils.secure_randoms import gen_secure_rand_token

SQL_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

# Body for POST /auth/authorize
AUTHORIZE_BODY = \
    [
        {
            'name': 'userCode',
            'required': True,
            'regex': r"[a-zA-Z0-9_-]{64}"
        },
        {
            'name': 'userToken',
            'required': True,
            'regex': r"[a-zA-Z0-9_-]{250}"
        }
    ]

# Body for GET /auth/authorize
AUTHORIZE_EMAIL_QUERY = \
    [
        {
            'name': 'userCode',
            'required': True,
            'regex': r"[a-zA-Z0-9_-]{64}"
        }
    ]

# Body for POST /auth/token
TOKEN_BODY = \
    [
        {
            'name': 'appCode',
            'required': True,
            'regex': r"[a-zA-Z0-9_-]{64}"
        },
        {
            'name': 'appName',
            'required': True,
            'regex': r"[a-zA-Z0-9 '-]{1,50}"
        }
    ]

# Allowed scopes in POST /auth/initialize
ALLOWED_SCOPES = "msisdn|email|firstname|lastname|birthDate|languages|nationality|address|gender"  # allowed scopes
SCOPES_NUMBER = len(ALLOWED_SCOPES.split('|'))  # used to avoid infinite strings and buffer overflow

# Body for POST /auth/initialize
INIT_AUTHORIZATION_BODY = \
    [
        {
            'name': 'appName',
            'required': True,
            'regex': r"[a-zA-Z0-9 '-]{1,50}"
        },
        {
            'name': 'login',
            'required': True,
            'regex': r"[^@]+@[^@]+\.[^@]+|[0-9]{9,15}"
        },
        {
            'name': 'notificationType',
            'required': False,
            'regex': r"email"
        },
        {
            'name': 'scopes',
            'required': True,
            # list of possible scopes with ' ' separator with a maximum of the number of scopes
            # (no problem if there is the same scope multiple times, we will remove duplicates)
            'regex': r"((" + ALLOWED_SCOPES + ") ){1," + str(SCOPES_NUMBER - 1) + "}(" + ALLOWED_SCOPES + "){1}"
        }
    ]


class AuthenticationController:
    """class AuthenticationController: class that will manage all requests with base path '/auth'

    """

    def __init__(self, pool: ObjectPool):
        """constructor

        :param pool: the PoolObject that contains all used instances
        """
        # init pool
        self.pool = pool

        # init polling cache that will be user to avoid having too much polling
        self.polling_cache = {}

    def init_authorization(self) -> Response:
        """init_auth method: controller for POST /auth/initialize. Endpoint used by third tier app to ask for
                                access to user's data

            :return: the response to send back to the client
        """

        # verify input is valid
        validation = verify_dict_body(request.json, INIT_AUTHORIZATION_BODY)
        if validation is not None:
            return validation

        # verify user exists
        user_id = self.pool.user_db.find_user_id_by_email(request.json.get('login'))
        if user_id is None:
            return unknown_user('email')

        # generate user_code and app_code
        user_code = gen_secure_rand_token(64)
        app_code = gen_secure_rand_token(64)

        # save request
        self.pool.codes_db.create_code(user_code,
                                       user_id,
                                       request.json.get('appName'),
                                       request.json.get('scopes'),
                                       app_code)

        # send email
        self.pool.email_manager.send_code_email(request.json.get('login'),
                                                request.json.get('appName'),
                                                user_code,
                                                request.json.get('scopes').split(' '))

        # init polling cache
        self.polling_cache[app_code] = int(datetime.now().timestamp())

        # return success with app_code availability of that code and minimum interval of polling
        return Response(json.dumps({
            "applicationCode": app_code,
            "availability": int(self.pool.time_limit_codes) * 60,
            "interval": int(self.pool.polling_interval)
        }),
            status=200,
            mimetype="application/json")

    def token(self) -> Response:
        """verify_auth method: controller for POST /auth/token. Endpoint used by third tier app to verify if
                                authorization to access user's data has been granted. If yes, the application will get
                                a token to get user's allowed data (scopes)

            :return: the response to send back to the client
        """
        # verify input
        validation = verify_dict_body(request.json, TOKEN_BODY)
        if validation is not None:
            return validation

        app_code = request.json.get("appCode")

        # manage cache
        if app_code in self.polling_cache:
            cache_plus_interval = self.polling_cache[app_code] + 5
            now = int(datetime.now().timestamp())

            # save new date to cache
            self.polling_cache[app_code] = now

            # verify interval
            if cache_plus_interval > now:
                return too_many_requests(self.pool.polling_interval)
        else:
            self.polling_cache[app_code] = int(datetime.now().timestamp())

        # get scopes
        scopes_user_id = self.pool.codes_db.get_scope_userid_by_authorized_appcode(request.json.get("appCode"),
                                                                                   request.json.get("appName"))
        if scopes_user_id is None:
            return unauthorized()
        scopes = scopes_user_id[0]
        user_id = scopes_user_id[1]

        # generate token for third tier application
        token = gen_secure_rand_token(128)

        # save token and delete codes
        self.pool.access_tokens_db.create_token(token, user_id, scopes)
        self.pool.codes_db.delete_appcode(request.json.get("appCode"))

        # remove from cache
        if app_code in self.polling_cache:
            del self.polling_cache[app_code]

        # return success with token, scope availability in seconds and associated scopes
        return Response(json.dumps({
            "availability": (self.pool.time_limit_tokens * 60),
            "access_token": token,
            "scopes": scopes
        }),
            status=200,
            mimetype="application/json")

    def authorize(self) -> Response:
        """authorize method: controller for POST /auth/authorize. Endpoint used by user to allow access to data
                                (via mobile app)

        :return: the response to send back to the client
        """
        # verify input
        validation = verify_dict_body(request.json, AUTHORIZE_BODY)
        if validation is not None:
            return validation

        # verify user_code exists
        user_code = request.json.get('userCode')
        exists = self.pool.codes_db.usercode_exists(user_code)
        if not exists:
            return unauthorized()

        # verify user_code hasn't been authorized (via POST or GET /auth/authorize)
        authorized = self.pool.codes_db.usercode_is_authorized(user_code)
        if authorized:
            return unauthorized()

        # set user_code authorized
        self.pool.codes_db.set_usercode_authorized(user_code)
        query_result = self.pool.codes_db.get_scope_username_appname_by_usercode(user_code)

        # return success with token validity in seconds
        return Response(
            json.dumps({
                "firstname": query_result[1],
                "app_name": query_result[2],
                "scopes": query_result[0].split(' '),
                "token_validity": self.pool.time_limit_tokens * 60
            }),
            status=200,
            mimetype="application/json"
        )

    def authorize_email(self) -> Response:
        """authorize_email method: controller for GET /auth/authorize. Endpoint used by user to allow access to data
                                (via email)

            :return: the response to send back to the client (html)
        """

        # verify input
        validation = verify_query(request.args, AUTHORIZE_EMAIL_QUERY)
        if validation is not None:
            return render_template('KO.html')

        # verify user_code exists
        user_code = request.args.get('userCode')
        exists = self.pool.codes_db.usercode_exists(user_code)
        if not exists:
            return render_template('KO.html')

        # verify user_code hasn't been authorized (via POST or GET /auth/authorize)
        authorized = self.pool.codes_db.usercode_is_authorized(user_code)
        if authorized:
            return render_template('KO.html')

        # set user_code authorized
        self.pool.codes_db.set_usercode_authorized(user_code)
        query_result = self.pool.codes_db.get_scope_username_appname_by_usercode(user_code)

        # return success template
        return render_template('OK.html',
                               firstname=query_result[1],
                               app_name=query_result[2],
                               scopes=query_result[0].split(' '),
                               token_validity_minutes=self.pool.time_limit_tokens)

    def get_waiting(self):
        """get_waiting method: controller for GET /auth/waiting. Endpoint used by user to see pending access requests
                                (via mobile app)

            :return: the response to send back to the client
        """
        # verify given token is valid
        scopes_and_userid = self.pool.security.verify_authorization(request.headers.get('Authorization'), True)
        if scopes_and_userid is None:
            return unauthorized()

        # create response
        user_id = scopes_and_userid[1]
        res = [{"userCode": tup[0], "date": str(tup[1]), "appName": tup[2], "scopes": tup[3].split(" ")} for tup in
               self.pool.codes_db.get_waiting_codes(user_id)]

        # return response
        return Response(json.dumps(res),
                        status=200,
                        mimetype="application/json")

    def get_allowed(self):
        """controller for GET /auth/allowed. Endpoint used by user to see allowed access requests
                                (via mobile app)

        :return: the response to send back to the client
        """
        # verify given token is valid
        scopes_and_userid = self.pool.security.verify_authorization(request.headers.get('Authorization'), True)
        if scopes_and_userid is None:
            return unauthorized()

        # create response
        user_id = scopes_and_userid[1]
        res = [{"date": str(tup[0]), "appName": tup[1], "scopes": tup[2].split(" "),
                "accessUntil": str(datetime.strptime(str(tup[0]), SQL_TIME_FORMAT)
                                   + timedelta(minutes=self.pool.time_limit_codes))}
               for tup in self.pool.codes_db.get_authorized_codes(user_id)]

        # return response
        return Response(json.dumps(res),
                        status=200,
                        mimetype="application/json")
