import src.database.database as database
import src.utils.email as email
from src.controller.scheduler import Scheduler
from src.utils.security import SecurityHelper


class ObjectPool:
    """class ObjectPool
    This class is a class for the object pool design pattern. This class is a pool of all instanciated classes that can
    be used by other classes. Instead of initializing on-demand classes for every use, we will have here an instance of
    each class (or 1 instance for each use if the class is not tread-safe). Most of the classes here have to
    be configured at start, so that will avoid having to instantiate each class and giving all the dependencies.
    Instead, we will just give them an instance of ObjectPool
    """

    def __init__(self, conf_dict):
        # config and init database connectors
        self.user_db = database.UserDatabaseConnector(conf_dict["DATABASE"])
        self.codes_db = database.CodesDatabaseConnector(conf_dict["DATABASE"])
        self.access_tokens_db = database.AccessTokensDatabaseConnector(conf_dict["DATABASE"])
        self.user_tokens_db = database.UserTokenDatabaseConnector(conf_dict["DATABASE"])

        # config and init emailManager
        self.email_manager = email.EmailManager(conf_dict["EMAIL"])

        # config manager
        self.scheduler = Scheduler(self, conf_dict["SCHEDULER"])

        # security helper
        self.security = SecurityHelper(self)

        # get expiration times
        self.time_limit_codes = int(conf_dict["VALIDITY"]["time_limit_codes_minutes"])
        self.time_limit_tokens = int(conf_dict["VALIDITY"]["time_limit_tokens_minutes"])
        self.polling_interval = int(conf_dict["VALIDITY"]["polling_interval_seconds"])
