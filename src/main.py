import configparser

from flask import Flask

import src.controller.authentication as auth
import src.controller.info as info
import src.controller.user as user
from src.error.errors import internal_error, not_found
from src.objectpool import ObjectPool

# init config from config file
parser = configparser.ConfigParser()
parser.read("../resources/conf.ini")
conf_dict = {section: dict(parser.items(section)) for section in parser.sections()}

# init object pool
pool = ObjectPool(conf_dict)

# init all controllers
info_controller = info.InfoController(conf_dict["INFO"])
user_controller = user.UsersController(pool)
authentication_controller = auth.AuthenticationController(pool)

api = Flask(__name__, template_folder='../resources/templates')

# info endpoints
#   get infos
api.add_url_rule(methods=['GET'], rule="/infos", view_func=info_controller.get_infos)

# users endpoints
#    create a user
api.add_url_rule(methods=['POST'], rule="/users", view_func=user_controller.create_user)
#    modify a user
# api.add_url_rule(methods=['PATCH'], rule="/users", view_func=user_controller.modify_user)
#    get a user's info
api.add_url_rule(methods=['GET'], rule="/users/info", view_func=user_controller.get_info)

# authentication endpoints
#    initialize authentication
api.add_url_rule(methods=['POST'], rule="/auth/initialize", view_func=authentication_controller.init_authorization)
#    get a token
api.add_url_rule(methods=['POST'], rule="/auth/token", view_func=authentication_controller.token)
#    authorize an application (email)
api.add_url_rule(methods=['GET'], rule="/auth/authorize", view_func=authentication_controller.authorize_email)
#    authorize an application (others)
api.add_url_rule(methods=['POST'], rule="/auth/authorize", view_func=authentication_controller.authorize)
#    get waiting authorizations
api.add_url_rule(methods=['GET'], rule="/auth/waiting", view_func=authentication_controller.get_waiting)
#    get allowed authorizations
api.add_url_rule(methods=['GET'], rule="/auth/allowed", view_func=authentication_controller.get_allowed)


# set error handlers

@api.errorhandler(404)
def resource_not_found(e):
    return not_found()


@api.errorhandler(500)
def unknown_error(e):
    return internal_error()


# start app
if __name__ == '__main__':
    # start API
    api.run(host='0.0.0.0')
