from flask import json, Response

MIME_TYPE = "application/json"


def invalid_json() -> Response:
    """get the response to send when the JSON of request is invalid

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "INVALID_JSON",
            "message": "The provided JSON is not valid."
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def user_already_exists(field: str) -> Response:
    """get the response to send when a user creation is done on a user already existing

    :param field: the field that already exists (email, msisdn...)
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "USER_" + field.upper() + "_ALREADY_EXISTS",
            "message": "User with that " + field + " already exists."
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def missing_field(field: str) -> Response:
    """get the response to send when there is a missing field in a request

    :param field: the missing field name
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "MISSING_FIELD",
            "message": "The request is missing the required field " + field
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def invalid_param(field: str, regex: str) -> Response:
    """get the response to send when an invalid query parameter is given

    :param field: the field name that is invalid
    :param regex: the regex that must match the field
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "INVALID_PARAMETER",
            "message": "The request has an invalid query parameter : " + field +
                       ". This field must match the following regex : " + regex
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def missing_param(field: str) -> Response:
    """get the response to send when there is a missing query parameter

    :param field: the field name that is missing
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "MISSING_PARAMETER",
            "message": "The request is missing the required query parameter " + field
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def invalid_field(field: str, regex: str) -> Response:
    """get the response to send when an invalid field in body is given

    :param field: the field name that is invalid
    :param regex: the regex that must match the field
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "INVALID_FIELD",
            "message": "The request has an invalid field : " + field +
                       ". This field must match the following regex : " + regex
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def invalid_date(date: str) -> Response:
    """get the response to send when an invalid date is given

    :param date:the date that is invalid
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "INVALID_DATE",
            "message": "The date " + date + " is an invalid date."
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def unknown_user(field: str) -> Response:
    """get the response to send when a user is unknown

    :param field: the field name that is unkown (email, msisdn...)
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "UNKNOWN_USER_" + field.upper(),
            "message": "No user with that " + field + " was found."
        }),
        status=400,
        mimetype=MIME_TYPE
    )


def unauthorized() -> Response:
    """get the response to send when an authentication failed

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "UNAUTHORIZED",
            "message": "Invalid or bad credentials."
        }),
        status=401,
        mimetype=MIME_TYPE
    )


def not_found() -> Response:
    """get the response to send when the given endpoint is unknown

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "NOT_FOUND",
            "message": "No resource found"
        }),
        status=404,
        mimetype=MIME_TYPE
    )


def too_many_requests(interval: int) -> Response:
    """get the response to send when a client is doing too much requests in a limited period of time

    :param interval: the minimal interval between each request
    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "TOO_MANY_REQUEST_WITH_THAT_APP_CODE",
            "message": "You sent too many requests in a limited period of time with that application code."
                       " Please refer to 'interval' field returned on initialization.",
            "interval": interval
        }),
        status=429,
        mimetype=MIME_TYPE
    )


def internal_error() -> Response:
    """get the response to send when an unexpected error occured

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "error_code": "INTERNAL_SERVER_ERROR",
            "message": "An unexpected error occurred."
        }),
        status=500,
        mimetype=MIME_TYPE
    )
