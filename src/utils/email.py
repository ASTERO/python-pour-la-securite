from flask import request
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

# translate scopes to human readable data
SCOPES_TO_NAMES = \
    {
        "msisdn": "Phone Number",
        "email": "Email",
        "firstname": "First name",
        "lastname": "Last name",
        "birthDate": "Birth date",
        "languages": "Languages you speaks",
        "nationality": "nationality",
        "address": "address",
        "gender": "gender"
    }


class EmailManager:
    """class EmailManager: class that will manage all emails sending

    """

    def __init__(self, config: dict):
        self.api_key = config['api_key']
        self.email_from = config['email_from']

    def send_code_email(self, email: str, client_name: str, code: str, scopes: list[str]) -> None:
        """send an email to authorize a request

        :param email: the email of the receiver
        :param client_name: the name of the application trying to access data
        :param code: the user code to authorize the request
        :param scopes: the scopes associated to that request
        :return: nothing
        """

        # create the email body
        content = 'Hi, ' + client_name + ' wants to have access to your:<br/>'

        all_data = [SCOPES_TO_NAMES[scope] for scope in scopes]

        for data in all_data:
            content += "   - " + data + '<br>'
        content += 'If you want to allow ' + client_name + ' to access your information, please follow that <a href="' \
                   + request.url_root + "auth/authorize?userCode=" + code + \
                   '">verification link</a> ! <br/> Else, just ignore that email !'

        # create the email
        message = Mail(
            from_email=self.email_from,
            to_emails=email,
            subject='Your verification link',
            html_content=content
        )

        # send it
        SendGridAPIClient(self.api_key).send(message)
