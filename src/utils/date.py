from datetime import datetime


def minus_years(years: int, from_date: datetime):
    """function to minus years to a datetime ensuring the date exists.

    :param years: int. The number of years to remove from the date
    :param from_date: datetime. The initial datetime
    :return: a datetime corresponding to from_date - years
    """
    try:
        return from_date.replace(year=from_date.year - years)
    except ValueError:
        return from_date.replace(month=2, day=28,
                                 year=from_date.year - years)
