class SecurityHelper:
    """class EmailManager: class that will manage all security (authentication, ...)

    """

    def __init__(self, pool):
        self.pool = pool

    def verify_authorization(self, authorization: str, scope_all: bool = False) -> str or None:
        """verifies if a token is valid and then returns its scopes and user_id

        :param authorization: the authorization header
        :param scope_all: True to only accept 'all' scoped token, False to accept any scope (default)
        :return: a tuple (user_id, scopes_of_token), or None if the token is not valid
        """

        # verify if token is not null
        if authorization is None:
            return None

        # verify if token looks like 'Bearer <something>
        split = authorization.split(' ')
        if len(split) != 2 or split[0] != 'Bearer':
            return None

        # if we only want to accept 'all' scoped tokens (user tokens) we verify that the token exists in user tokens db
        if scope_all:
            user_id = self.pool.user_tokens_db.userid_from_token(split[1])
            return (['all'], user_id) if user_id is not None else None

        # else we can accept both user token or access token
        # first we try to get it as an access token
        scopes_and_userid = self.pool.access_tokens_db.scopes_userid_from_token(split[1])
        if scopes_and_userid is None:
            # if we didn't get it we try to get it as user token and send back the result
            user_id = self.pool.user_tokens_db.userid_from_token(split[1])
            return (['all'], user_id) if user_id is not None else None

        # else we just send back the scopes of that access token and the user ID
        return scopes_and_userid[0].split(' '), scopes_and_userid[1]
