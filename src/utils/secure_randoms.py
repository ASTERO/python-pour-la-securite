import secrets
import string


def gen_secure_rand_token(size: int) -> str:
    """generate secure random tokens of given size

    :param size: the size of the token
    :return: the generated token
    """
    return secrets.token_urlsafe(int(size / 1.2))[:size]


def gen_secure_rand_str_int(digits: int) -> str:
    """generate secure random numbers (as strings)

    :param digits: the number of digits of the generated number
    :return: the generated number
    """
    return ''.join(secrets.choice(string.digits) for i in range(digits))
