import re

from werkzeug.datastructures import ImmutableMultiDict

from src.error.errors import missing_field, invalid_json, invalid_field, missing_param, invalid_param


def verify_dict_body(json: dict, fields: list[dict], verify_required: bool = True):
    """function verify_dict_body: verifies that an object is a JSON object (not an array or other) and verifies fields

    :param fields: a list of dict representing field composed of:
        name: str, the name of the field;
        required: bool, is the field is mandatory;
        regex: str, the regex to match or None if no regex to match
    :param json: the parsed JSON to verify
    :param verify_required: True (default) to verify if required fields are present,
            False if you don't want to verify required parameters
    :return: the response to send if something is wrong, None otherwise
    """
    print(json)
    if (json is None) or (type(json) is not type({})):
        return invalid_json()

    for field in fields:
        if field["required"] is True and field["name"] not in json.keys():
            return missing_field(field["name"])

        if field["name"] in json.keys() and not re.fullmatch(field["regex"], json[field["name"]]):
            return invalid_field(field["name"], field["regex"])


def verify_query(query: ImmutableMultiDict, params: list[dict]):
    """function verify_query: verifies that a query param has the right fields

    :param params: a list of dict representing field composed of:
        name: str, the name of the field;
        required: bool, is the field is mandatory;
        regex: str, the regex to match or None if no regex to match
    :param query: the query param
    :return: the response to send if something is wrong, None otherwise
    """
    for field in params:
        if field["required"] is True and field["name"] not in query:
            return missing_param(field["name"])
        if field["name"] in query and not re.fullmatch(field["regex"], query.get(field["name"])):
            return invalid_param(field["name"], field["regex"])
