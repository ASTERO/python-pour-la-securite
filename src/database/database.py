from datetime import datetime, timedelta

import mysql.connector
from mysql.connector import MySQLConnection

SQL_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

# translate from scope names to database columns names
SCOPES_TO_DATABASE_ROWS = \
    {
        "msisdn": "MSISDN",
        "email": "EMAIL",
        "firstname": "FIRSTNAME",
        "lastname": "LASTNAME",
        "birthDate": "BIRTH_DATE",
        "languages": "LANGUAGES",
        "nationality": "NATIONALITY",
        "address": "ADDRESS",
        "gender": "GENDER"
    }


class DatabaseConnector:
    """class DatabaseConnector: parent class of all classes that will have to connect to database

    """

    def __init__(self, database_conf: dict):
        self.database_conf = database_conf

    def init_connection(self) -> MySQLConnection:
        return mysql.connector.connect(user=self.database_conf['user'],
                                       password=self.database_conf['password'],
                                       host=self.database_conf['host'],
                                       database=self.database_conf['database'])


class AccessTokensDatabaseConnector(DatabaseConnector):
    """class AccessTokensDatabaseConnector: class that will manage all database operations on tokens (allowed requests)

    """

    def scopes_userid_from_token(self, token: str) -> tuple[str]:
        """get scopes and user ID from a token

        :param token: the token to query
        :return: a tuple (scopes, user ID)
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(operation="SELECT SCOPES, USER_ID FROM access_tokens WHERE TOKEN = %s;", params=(token,))
        res = cursor.fetchone()
        return res

    def delete_expired(self, expiration: int):
        """delete expired tokens

        :param expiration: the expiration time in minutes
        :return: nothing
        """
        db = self.init_connection()

        db.cursor().execute(
            "DELETE FROM access_tokens WHERE `TIMESTAMP` < %s",
            ((datetime.now() - timedelta(minutes=expiration)).strftime(SQL_TIME_FORMAT),))
        db.commit()

    def create_token(self, token: str, user_id: int, scopes: str):
        """create a token in database with the given info

        :param token: the generated token
        :param user_id: the id of the user
        :param scopes: the scopes associated to that token
        :return: nothing
        """
        db = self.init_connection()
        db.cursor().execute(
            "INSERT INTO access_tokens (TOKEN, USER_ID, `TIMESTAMP`, SCOPES) VALUES (%s, %s, %s, %s);",
            (token, user_id, datetime.now().strftime(SQL_TIME_FORMAT), scopes))
        db.commit()


class UserTokenDatabaseConnector(DatabaseConnector):
    """class UserTokenDatabaseConnector: class that will manage all database operations on user tokens
            (unique token given to user on creation that authenticates him)

    """

    def create_token(self, token: str, user_id: int):
        """create a user token in database with the given info

        :param token: the generated token
        :param user_id: the id of the user
        :return: nothing
        """
        db = self.init_connection()
        db.cursor().execute(
            "INSERT INTO user_tokens (TOKEN, USER_ID) VALUES (%s, %s);",
            (token, user_id))
        db.commit()

    def userid_from_token(self, token: str) -> str:
        """get user ID from a token

        :param token: the token to query
        :return: the user ID associated with that user token
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(operation="SELECT USER_ID FROM user_tokens WHERE TOKEN = %s;", params=(token,))
        res = cursor.fetchone()

        return res[0] if res is not None else None


class UserDatabaseConnector(DatabaseConnector):
    """class UserDatabaseConnector: class that will manage all database operations on users

    """

    def get_user_info(self, user_id: str, scopes: list[str]) -> dict[str, str]:
        """get user info according to the given scopes

        :param user_id: the ID of the user
        :param scopes: the scopes to get (info)
        :return: the user ID associated with that user token
        """
        # if it's a user token, scopes is all scopes available
        if scopes[0] == 'all':
            scopes = SCOPES_TO_DATABASE_ROWS.keys()

        # translate from scope to column name
        column_names = [SCOPES_TO_DATABASE_ROWS[scope] for scope in scopes]

        query = "SELECT {} FROM users WHERE ID = %s;".format(', '.join(column_names))

        # get all information in database
        db = self.init_connection()
        cursor = db.cursor()
        cursor.execute(operation=query, params=(user_id,))
        row = cursor.fetchone()

        # create response from scopes (request is in the same order as given scopes)
        res = {}
        for scope, val in zip(scopes, row):
            if scope == 'birthDate':
                res[scope] = str(val)
            else:
                res[scope] = val

        return res

    def create_user(self, user: dict) -> str:
        """creates a user in the database and return its ID

        :param user: the user to create
        :return: the user ID
        """
        db = self.init_connection()
        db.cursor().execute(
            "INSERT INTO users (MSISDN, LASTNAME, FIRSTNAME, EMAIL, BIRTH_DATE) VALUES (%s, %s, %s, %s, %s);",
            (user['msisdn'], user['lastname'], user['firstname'], user['email'], user['birthDate']))
        db.commit()

        db = self.init_connection()
        cursor = db.cursor()
        cursor.execute(operation="SELECT ID FROM users WHERE MSISDN = %s;", params=(str(user["msisdn"]),))
        res = cursor.fetchone()
        return res[0] if res is not None else None

    def user_exists_by_email(self, email: str = None) -> bool:
        """method user_exists_by_email: find if a user exists by email

        :param email: the email to search for
        :return: True if one or more user was found for that email, False otherwise
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute("SELECT * FROM users WHERE EMAIL = %s;", email)
        return len(cursor.fetchall()) > 0

    def user_exists_by_msisdn(self, msisdn: str = None) -> bool:
        """method user_exists_by_email: find if a user exists by email

        :param msisdn: the msisdn to search for
        :return: True if one or more user was found for that msisdn, False otherwise
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute("SELECT * FROM users WHERE MSISDN = %s;", msisdn)
        return len(cursor.fetchall()) > 0

    def find_user_id_by_email(self, email: str) -> int:
        """get user ID corresponding to that email

        :param email: the email to query
        :return: the user ID associated to that email, None if the email is unknown
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(operation="SELECT ID FROM users WHERE EMAIL = %s;", params=(email,))
        res = cursor.fetchone()
        return res[0] if res is not None else None

    def find_user_id_by_msisdn(self, msisdn: str) -> int:
        """get user ID corresponding to that msisdn (international phone number)

        :param msisdn: the msisdn to query
        :return: the user ID associated to that msisdn, None if the email is unknown
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(operation="SELECT ID FROM users WHERE MSISDN = %s;", params=(msisdn,))
        res = cursor.fetchone()
        return res[0] if res is not None else None


class CodesDatabaseConnector(DatabaseConnector):
    """class CodesDatabaseConnector: class that will manage all database operations on codes (pending requests)

    """

    def get_waiting_codes(self, user_id: str) -> dict[str]:
        """get authorization that are currently pending for that user

        :param user_id: the ID of the user
        :return: a list of tuple (user_code, timestamp of access, app name, scopes)
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(
            operation="SELECT CODE, `TIMESTAMP`, APP_NAME, SCOPES FROM codes WHERE USER_ID = %s AND AUTHORIZED = '0' ORDER BY `TIMESTAMP`;",
            params=(user_id,))
        return cursor.fetchall()

    def create_code(self, user_code: str, user_id: int, app_name: str, scopes: str, app_code: str) -> None:
        """create a code to identify that authorization request

        :param user_code: the generated user code
        :param user_id: the user ID associated to that request
        :param app_name: the application name
        :param scopes: the scopes associated to that request
        :param app_code: the generated app code
        :return: nothing
        """
        db = self.init_connection()
        db.cursor().execute(
            """INSERT INTO codes (CODE, USER_ID, APP_NAME, `TIMESTAMP`, SCOPES, APP_CODE)
                VALUES (%s, %s, %s, %s, %s, %s);""",
            (user_code, user_id, app_name, datetime.now().strftime(SQL_TIME_FORMAT), scopes, app_code))
        db.commit()

    def delete_expired(self, expiration: int):
        """delete expired codes

        :param expiration: the expiration time in minutes
        :return: nothing
        """
        db = self.init_connection()

        db.cursor().execute(
            "DELETE FROM codes WHERE `TIMESTAMP` < %s AND AUTHORIZED = 0;",
            ((datetime.now() - timedelta(minutes=expiration)).strftime(SQL_TIME_FORMAT),))
        db.commit()

    def usercode_exists(self, user_code: str) -> bool:
        """know if a user code exists

        :param user_code: the user code to query
        :return: True if it exists, False otherwise
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(operation="SELECT APP_CODE FROM codes WHERE CODE = %s;", params=(user_code,))
        res = cursor.fetchone()
        return True if res is not None and res[0] is not None else False

    def set_usercode_authorized(self, user_code: str):
        """mark a request as authorized

        :param user_code: the user code of the request
        :return: nothing
        """
        db = self.init_connection()

        db.cursor().execute(
            "UPDATE codes SET AUTHORIZED = 1 WHERE CODE = %s;""", params=(user_code,))
        db.commit()

    def usercode_is_authorized(self, user_code: str):
        """know if a request is authorized

        :param user_code: the user code of the request
        :return: True if it is authorized, False otherwise
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(operation="SELECT APP_CODE FROM codes WHERE CODE = %s AND AUTHORIZED = 1;",
                       params=(user_code,))
        res = cursor.fetchone()
        return True if res is not None and res[0] is not None else False

    def get_scope_userid_by_authorized_appcode(self, app_code: str, app_name: str) -> tuple[str]:
        """get the scopes and user ID associated to a request that is authorized

        :param app_code: the app code of the request
        :param app_name: the app name that does that request
        :return: None if the request with both app_code and app_name can't be found or that request is not authorized,
        returns a tuple (scopes, user ID) otherwise
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(
            operation="SELECT SCOPES, USER_ID FROM codes WHERE APP_CODE = %s AND APP_NAME = %s AND AUTHORIZED = 1;",
            params=(app_code, app_name))
        res = cursor.fetchone()
        return res

    def get_scope_username_appname_by_usercode(self, user_code: str) -> tuple[str]:
        """get the scopes, user's firstname, and app name associated to a request

        :param user_code: the user code of the request
        :return: None if the request with that user_code can't be found,
        returns a tuple (scopes, firstname, app_name) otherwise
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(
            operation="""SELECT codes.SCOPES, users.FIRSTNAME, codes.APP_NAME FROM codes 
                            JOIN users ON users.ID = codes.USER_ID WHERE CODE = %s;""",
            params=(user_code,))
        res = cursor.fetchone()
        return res

    def delete_appcode(self, app_code: str):
        """delete a request

        :param app_code: the app_code of the request
        :return: nothing
        """
        db = self.init_connection()

        db.cursor().execute(
            "DELETE FROM codes WHERE APP_CODE = %s;", params=(app_code,))
        db.commit()

    def get_authorized_codes(self, user_id: str) -> dict[str]:
        """get the authorized requests of a user

        :param user_id: the ID of the user
        :return: a list of tuple (timestamp of access, app name, scopes)
        """
        db = self.init_connection()
        cursor = db.cursor()

        cursor.execute(
            operation="SELECT `TIMESTAMP`, APP_NAME, SCOPES FROM codes WHERE USER_ID = %s AND AUTHORIZED = '1' ORDER BY `TIMESTAMP` DESC;",
            params=(user_id,))
        return cursor.fetchall()
